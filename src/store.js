import { observable } from "mobx";
import { getConnectAccount } from "./utils/wallet";

const help = () => {
    const wallet_is_show = observable.box(false);
    const currentAccount = observable.box("");
    const handleAccountChange = (accounts) => {
        if (accounts.length === 0) {
            console.log("accounts is null");
            currentAccount.set("");
        } else if (accounts[0] !== currentAccount) {
            console.log("accounts is " + accounts[0]);
            currentAccount.set(accounts[0]);
        }
    }

    const registerAccountChange = () => {
        console.log("registerAccountChange");
        const { ethereum } = window;

        getConnectAccount()
            .then((accounts) => {
                handleAccountChange(accounts);
            })
            .catch((err) => {
                console.error(err);
            });

        ethereum.on('accountsChanged', (accounts) => {
            handleAccountChange(accounts);
        });
    }

    return {wallet_is_show, registerAccountChange, currentAccount };

};

export const Store = help();
