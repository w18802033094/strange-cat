import { useEffect, useState } from 'react';
import { Store } from './store';
import { isMetaMaskInstalled, metamaskConnect, getContract, contractMint } from './utils/wallet';
import './App.css';
import Pic1 from './images/pic-1.png';
import Pic2 from './images/pic-2.png';
import Pic3 from './images/pic-3.png';
import Pic4 from './images/pic-4.png';
import Pic5 from './images/pic-5.png';
import Pic6 from './images/pic-6.png';
import Pic7 from './images/pic-7.png';
import Pic8 from './images/pic-8.png';
import Pic9 from './images/pic-9.png';
import Pic10 from './images/pic-10.png';
import Pic11 from './images/pic-11.png';
import Pic12 from './images/pic-12.png';

const metaMask_click = async (e) => {
  e.stopPropagation();
  if (isMetaMaskInstalled()) {
      metamaskConnect();
  }
}

const mint = (count, price) => {
  contractMint(count, price);
}

function App() {
  const [ isShowMint, setShowMint ] = useState(false);
  const [ mintInfo, setMintInfo ] = useState(null);
  const [ mintCount, setMintCount ] = useState(5);

  useEffect(() => {
    getContract()
      .then((info) => {
        setMintInfo(info);
      })
      .catch(err => {});
  }, []);

  useEffect(() => {
    Store.currentAccount.observe_((newAccount) => {
        if (newAccount.newValue === "") {
            setShowMint(false);
        } else {
            setShowMint(true);
        }
    });
  }, []);

  useEffect(() => {
    Store.registerAccountChange();
  }, []);

  return (
    <div className="App">
      <div className='icons'>
          <a className="link-icon" href="https://twitter.com/StrangeCatNFT" title="Twitter" rel="noreffer noopener" target="_blank">
            <svg className="icon w-6 h-6 sm:w-8 sm:h-8" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>Twitter</title><path d="M23.953 4.57a10 10 0 01-2.825.775 4.958 4.958 0 002.163-2.723c-.951.555-2.005.959-3.127 1.184a4.92 4.92 0 00-8.384 4.482C7.69 8.095 4.067 6.13 1.64 3.162a4.822 4.822 0 00-.666 2.475c0 1.71.87 3.213 2.188 4.096a4.904 4.904 0 01-2.228-.616v.06a4.923 4.923 0 003.946 4.827 4.996 4.996 0 01-2.212.085 4.936 4.936 0 004.604 3.417 9.867 9.867 0 01-6.102 2.105c-.39 0-.779-.023-1.17-.067a13.995 13.995 0 007.557 2.209c9.053 0 13.998-7.496 13.998-13.985 0-.21 0-.42-.015-.63A9.935 9.935 0 0024 4.59z"></path></svg>
          </a>
          <a className="link-icon" href="https://opensea.io/collection/strangecatsnft" title="OpenSea" rel="noreffer noopener" target="_blank">
            <svg className="w-6 h-6 sm:w-8 sm:h-8" role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>OpenSea</title><path d="M12 0C5.374 0 0 5.374 0 12s5.374 12 12 12 12-5.374 12-12S18.629 0 12 0ZM5.92 12.403l.051-.081 3.123-4.884a.107.107 0 0 1 .187.014c.52 1.169.972 2.623.76 3.528-.088.372-.335.876-.614 1.342a2.405 2.405 0 0 1-.117.199.106.106 0 0 1-.09.045H6.013a.106.106 0 0 1-.091-.163zm13.914 1.68a.109.109 0 0 1-.065.101c-.243.103-1.07.485-1.414.962-.878 1.222-1.548 2.97-3.048 2.97H9.053a4.019 4.019 0 0 1-4.013-4.028v-.072c0-.058.048-.106.108-.106h3.485c.07 0 .12.063.115.132-.026.226.017.459.125.67.206.42.636.682 1.099.682h1.726v-1.347H9.99a.11.11 0 0 1-.089-.173l.063-.09c.16-.231.391-.586.621-.992.156-.274.308-.566.43-.86.024-.052.043-.107.065-.16.033-.094.067-.182.091-.269a4.57 4.57 0 0 0 .065-.223c.057-.25.081-.514.081-.787 0-.108-.004-.221-.014-.327-.005-.117-.02-.235-.034-.352a3.415 3.415 0 0 0-.048-.312 6.494 6.494 0 0 0-.098-.468l-.014-.06c-.03-.108-.056-.21-.09-.317a11.824 11.824 0 0 0-.328-.972 5.212 5.212 0 0 0-.142-.355c-.072-.178-.146-.339-.213-.49a3.564 3.564 0 0 1-.094-.197 4.658 4.658 0 0 0-.103-.213c-.024-.053-.053-.104-.072-.152l-.211-.388c-.029-.053.019-.118.077-.101l1.32.357h.01l.173.05.192.054.07.019v-.783c0-.379.302-.686.679-.686a.66.66 0 0 1 .477.202.69.69 0 0 1 .2.484V6.65l.141.039c.01.005.022.01.031.017.034.024.084.062.147.11.05.038.103.086.165.137a10.351 10.351 0 0 1 .574.504c.214.199.454.432.684.691.065.074.127.146.192.226.062.079.132.156.19.232.079.104.16.212.235.324.033.053.074.108.105.161.096.142.178.288.257.435.034.067.067.141.096.213.089.197.159.396.202.598a.65.65 0 0 1 .029.132v.01c.014.057.019.12.024.184a2.057 2.057 0 0 1-.106.874c-.031.084-.06.17-.098.254-.075.17-.161.343-.264.502-.034.06-.075.122-.113.182-.043.063-.089.123-.127.18a3.89 3.89 0 0 1-.173.221c-.053.072-.106.144-.166.209-.081.098-.16.19-.245.278-.048.058-.1.118-.156.17-.052.06-.108.113-.156.161-.084.084-.15.147-.208.202l-.137.122a.102.102 0 0 1-.072.03h-1.051v1.346h1.322c.295 0 .576-.104.804-.298.077-.067.415-.36.816-.802a.094.094 0 0 1 .05-.03l3.65-1.057a.108.108 0 0 1 .138.103z"></path></svg>
          </a>
          <a className="link-icon" href="https://etherscan.io/address/0x60e2a8efc87357deea18129d39501d47c4d4d6f6" title="Etherscan" rel="noreffer noopener" target="_blank">
            <svg xmlns="http://www.w3.org/2000/svg" className="w-6 h-6 sm:w-8 sm:h-8" viewBox="0 0 293.775 293.667">
              <g id="etherscan-logo-light-circle" transform="translate(-219.378 -213.333)">
                <path id="Path_1" data-name="Path 1" d="M280.433,353.152A12.45,12.45,0,0,1,292.941,340.7l20.737.068a12.467,12.467,0,0,1,12.467,12.467v78.414c2.336-.692,5.332-1.43,8.614-2.2a10.389,10.389,0,0,0,8.009-10.11V322.073a12.469,12.469,0,0,1,12.467-12.47h20.779a12.47,12.47,0,0,1,12.467,12.47v90.276s5.2-2.106,10.269-4.245a10.408,10.408,0,0,0,6.353-9.577V290.9a12.466,12.466,0,0,1,12.465-12.467h20.779A12.468,12.468,0,0,1,450.815,290.9v88.625c18.014-13.055,36.271-28.758,50.759-47.639a20.926,20.926,0,0,0,3.185-19.537,146.6,146.6,0,0,0-136.644-99.006c-81.439-1.094-148.744,65.385-148.736,146.834a146.371,146.371,0,0,0,19.5,73.45,18.56,18.56,0,0,0,17.707,9.173c3.931-.346,8.825-.835,14.643-1.518a10.383,10.383,0,0,0,9.209-10.306V353.152" transform="translate(0 0)"></path>
                <path id="Path_2" data-name="Path 2" d="M244.417,398.641A146.808,146.808,0,0,0,477.589,279.9c0-3.381-.157-6.724-.383-10.049-53.642,80-152.686,117.405-232.79,128.793" transform="translate(35.564 80.269)"></path>
              </g>
            </svg>
          </a>
        </div>
      <div className='content'>
        <div className='title'>Strange Cat</div>
        <div className='mint'>
          <div className="mint-text"> { mintInfo ? mintInfo.current : '0 / 0' } </div>
          {!isShowMint ? <div className="mint-btn" onClick={(e) => { metaMask_click(e) }}>CONNECT</div> : null}
          {isShowMint ?
            <div className='u-d-btns'>
              <span className="u-d-btn-1" onClick={() => mintCount > 1 ? setMintCount(mintCount - 1) : 1 }>{'<'}</span>
              <div className="mint-btn-small" onClick={(e) => { mint(mintCount, mintInfo.cost ? mintInfo.cost * mintCount : 0.002 * mintCount) }}>MINT: {mintCount}</div>
              <span className="u-d-btn-2" onClick={() => mintCount < 5 ? setMintCount(mintCount + 1) : 5 }>{'>'}</span>
            </div> 
            : null}
        </div>
      </div>
      <div className='images'>
        <div>
          <img src={Pic1} alt=''></img>
          <img src={Pic2} alt=''></img>
          <img src={Pic3} alt=''></img>
          <img src={Pic4} alt=''></img>
        </div>
        <div>
          <img src={Pic5} alt=''></img>
          <img src={Pic6} alt=''></img>
          <img src={Pic7} alt=''></img>
          <img src={Pic8} alt=''></img>
        </div>
        <div>
          <img src={Pic9} alt=''></img>
          <img src={Pic10} alt=''></img>
          <img src={Pic11} alt=''></img>
          <img src={Pic12} alt=''></img>
        </div>
      </div>
    </div>
  );
}

export default App;
